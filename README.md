
# Breezy Toolchain

A Yocto toolchain to make preparing builds for selected SoCs a breeze.  

The repository provides convenience tools and scripts to simplify:
* fetching all required yocto layers for a supported target
* configuring the build environment for a supported target
* deploy builds to a supported target

## Supported Targets
The toolchain aims to support multiple targets. Currently supported targets are:

* Raspberry Pi 4 64-bit

## Instructions

### Toolchain Configuration
The toolchain needs to be configured for the desired target. Each configuration of the toolchain is tuned to reliably support a single specified target. Targets within the same family may successfully build in related toolchain configurations, however results may vary and this is not recommended.

To configure the toolchain, simply run the provided script: 
```
configure-target.sh
```

The script will prompt you for the desired target, and once selected, will pull all required external dependencies.

### Environment Setup
Once the toolchain is configured, the environment can be prepared for building. This process is an analog to the usual `setup-oe-environment` usage when building with poky.

Simply run: 
```
source setup-environment
``` 

The toolchain should auto detect the configured target and set up the default build environment and layer topology.
If you would like to specify a non-default DISTRO (i.e. something custom-made) this can be done as follows: 
```
DISTRO=MY_DISTRO source setup-environment
``` 

### Deploying Images
The toolchain provides tools to quickly deploy newly built images to your target, aiming to make the process as effortless as possible, and speed up development iterations.

This feature is still a work-in-progress.

## Additional Features
* basic distro and image definitions are included in the toolchain as a reference
* default `fstab` and `interface` configurations are provided for supported targets