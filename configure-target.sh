
########## Environment ##########

WORKDIR=`dirname $(realpath -s $0)`

########## Helpers ##########

function check_if_configured () {
    if [[ -d $WORKDIR/.repo/ ]]
    then
        echo "Project is already configured. Clean before re-configuring."
        exit 1
    fi
}

function clean () {
    echo "Cleaning current project configuration..."
    rm -rf $WORKDIR/.repo/
    rm -rf $WORKDIR/yocto-layers/external/*
    rm -rf $WORKDIR/MACHINE
    echo "Done"
}

function configure_pi4 () {
    echo "Configuring for Raspberry Pi 4 64-bit"
    repo init -u git@gitlab.com:ithier/breezy-toolchain.git -m manifests/raspberrypi-4-64.xml
    repo sync
}

function configure_qemu () {
    echo "Configuring for qemux86_64"
    repo init -u git@gitlab.com:ithier/breezy-toolchain.git -m manifests/qemux86-64.xml
    repo sync
}

########## Main Loop ##########

PS3='Select your target:'
options=(
    "qemux86_64"
    "Raspberry Pi 4 64-bit" 
    "Beagle Bone Black"
    "Clean"
    "Quit")

select opt in "${options[@]}"
do
    case $opt in
	"qemux86_64")
	    check_if_configured
	    configure_qemu
	    echo "qemux86-64" > MACHINE
	    echo "breezy" > DISTRO
        break
        ;;
    "Raspberry Pi 4 64-bit")
        check_if_configured
        configure_pi4
	    echo "raspberrypi4-64" > MACHINE
        echo "breezy" > DISTRO
        break
        ;;
    "Beagle Bone Black")
        break
	    echo "Not yet supported."
	    exit 2
        ;;
    "Clean")
        clean
        break
        ;;
    "Quit")
        break
        ;;
    *) 
        echo "Invalid selection"
        ;;
    esac
done
